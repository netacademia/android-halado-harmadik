# NetAcademia-Android-Harmadik

A videó során meglátogatott linkek

## Global
* Android dashboard https://developer.android.com/about/dashboards/
* Activity lifecycle https://www.javatpoint.com/images/androidimages/Android-Activity-Lifecycle.png
* Kotlin this... https://kotlinlang.org/docs/reference/this-expressions.html
* DisposableManager implementáció https://medium.com/@CodyEngel/managing-disposables-in-rxjava-2-for-android-388722ae1e8a
* Kotlin filewriter https://stackoverflow.com/questions/35444264/how-do-i-write-to-a-file-in-kotlin/35445704
* Android temp file https://stackoverflow.com/questions/3425906/creating-temporary-files-in-android


## Libek
* Support libek https://developer.android.com/topic/libraries/support-library/packages
* Butterknife https://github.com/JakeWharton/butterknife
* JSR-310 backport https://github.com/JakeWharton/ThreeTenABP
* Evernote Job https://github.com/evernote/android-job
* RxJava https://github.com/ReactiveX/RxJava
* RxAndroid https://github.com/ReactiveX/RxAndroid


## Guide oldalak

* WorkManager https://developer.android.com/topic/libraries/architecture/workmanager
* Notification Custom layout - remote view https://developer.android.com/reference/android/widget/RemoteViews
* Services https://developer.android.com/guide/components/services
* Threading https://developer.android.com/training/multiple-threads/run-code
* Threading 2 https://developer.android.com/guide/components/processes-and-threads#Threads



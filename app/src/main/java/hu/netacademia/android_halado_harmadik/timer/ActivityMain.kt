package hu.netacademia.android_halado_harmadik.timer

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import hu.netacademia.android_halado_harmadik.R
import hu.netacademia.android_halado_harmadik.job.DemoJob
import hu.netacademia.android_halado_harmadik.job.DummyService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ActivityMain : AppCompatActivity() {

    @JvmField
    @BindView(R.id.counter_start_button)
    var startButton: Button? = null

    @JvmField
    @BindView(R.id.counter_text)
    var counterText: TextView? = null

    @JvmField
    @BindView(R.id.counter_progress)
    var counterProgress: ProgressBar? = null

    val disposableManager = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        startButton?.setOnClickListener {
            startCountingWithRx()
            DemoJob().scheduleJob()
        }
    }

    /**
     * TODO
     * ui blocking counter
     */
    private fun startCountingBlocking() {
        var currentCount = 1
        while (currentCount < 6) {
            try {
                Thread.sleep(TimeUnit.SECONDS.toMillis(3))
            } catch (e: InterruptedException) {
                //handle
            }
            counterText?.text = currentCount.toString()
            currentCount++
        }
    }

    private fun startCountingWithThreads() {
        val handler = Handler()
        Thread {
            var currentCount = 0
            while (currentCount < 6) {
                Thread.sleep(TimeUnit.SECONDS.toMillis(1))
                handler.post {
                    counterText?.text = currentCount.toString()
                }
                currentCount++
            }
        }.start()
    }

    private fun startCountingWithAsyncTask() {
        AsyncDummy().execute()
    }

    inner class AsyncDummy : AsyncTask<Void, Int, Void>() {

        override fun onProgressUpdate(vararg values: Int?) {
            super.onProgressUpdate(*values)
            this@ActivityMain.counterText?.text = values[0].toString()
        }

        override fun doInBackground(vararg voids: Void): Void? {
            var currentCount = 1
            while (currentCount < 6) {
                try {
                    Thread.sleep(TimeUnit.SECONDS.toMillis(3))
                } catch (e: InterruptedException) {
                    //handle
                }
                publishProgress(currentCount)
                currentCount++
            }
            return null
        }
    }

    private fun startCountingWithRx() {
        val notificationManager = NotificationManager(this)
        disposableManager.add(
                Observable
                        .interval(3, TimeUnit.SECONDS)
                        .take(5)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { currentCount ->
                            counterText?.text = currentCount.toString()
                            notificationManager.post(notificationManager.create(currentCount.toInt()))
                        }
        )
    }


    override fun onStop() {
        super.onStop()
        disposableManager.dispose()
    }

    fun startDummyService() {
        val intent = Intent(this, DummyService::class.java)
        startService(intent)
    }
}
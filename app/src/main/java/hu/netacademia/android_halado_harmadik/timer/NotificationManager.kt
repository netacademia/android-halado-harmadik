package hu.netacademia.android_halado_harmadik.timer

import android.annotation.TargetApi
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.support.v4.app.NotificationCompat
import hu.netacademia.android_halado_harmadik.R

class NotificationManager(private var context: Context) {

    private val manager: NotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as android.app.NotificationManager

    init {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            manager.createNotificationChannel(createChannel())
        }
    }

    fun create(counter: Int): Notification {
        return NotificationCompat.Builder(context, "noti channel")
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle(context.getString(R.string.counter))
                .setContentText(counter.toString())
                .build()
    }


    fun post(notification: Notification) {
        manager.notify(123, notification)
    }

    @TargetApi(Build.VERSION_CODES.O)
    private fun createChannel(): NotificationChannel {

        val notificationChannel = NotificationChannel("noti channel", "netacademia channel", NotificationManager.IMPORTANCE_LOW)
        notificationChannel.description = "channel for netacademia notifications"
        notificationChannel.enableLights(false)
        return notificationChannel
    }
}
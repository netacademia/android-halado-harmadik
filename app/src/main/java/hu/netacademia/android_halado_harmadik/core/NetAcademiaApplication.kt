package hu.netacademia.android_halado_harmadik.core

import android.app.Application
import com.evernote.android.job.JobManager
import com.jakewharton.threetenabp.AndroidThreeTen
import hu.netacademia.android_halado_harmadik.job.DemoJobCreator


class NetAcademiaApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        JobManager.create(this).addJobCreator(DemoJobCreator())
        AndroidThreeTen.init(this)
    }
}
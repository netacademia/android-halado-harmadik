package hu.netacademia.android_halado_harmadik.job

import android.util.Log
import com.evernote.android.job.Job
import com.evernote.android.job.JobRequest
import org.threeten.bp.LocalDateTime
import java.io.File


class DemoJob : Job() {

    companion object {
        val ID_TAG = DemoJob::class.java.simpleName
    }

    override fun onRunJob(params: Params): Result {
        doStuff()
        return Result.SUCCESS
    }

    private fun doStuff() {
        Log.d(ID_TAG, "job ran")
        val outPutDirectory = context.externalCacheDir
        val file = File.createTempFile("Netacademia ${LocalDateTime.now()}", ".txt", outPutDirectory)
        file.setWritable(true)
        file.bufferedWriter().use { out ->
            out.write("hello world!")
        }
    }

    fun scheduleJob() {
        JobRequest.Builder(DemoJob.ID_TAG)
                .setExecutionWindow(15000L, 15000L)
                .build()
                .schedule()
    }
}
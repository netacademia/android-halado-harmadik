package hu.netacademia.android_halado_harmadik.job

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator

class DemoJobCreator : JobCreator {

    override fun create(tag: String): Job? {
        return when (tag) {
            DemoJob.ID_TAG -> DemoJob()
            else -> null
        }
    }
}
